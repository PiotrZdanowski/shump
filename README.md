## Used plugins
Zenject/Extenject
Text Mesh Pro

## Project status
WIP (nearly all systems are creaded from scratch)
Implemented:
→ scenes flow
→ Player movement and input
→ Enemies movement and spawning (simplified)
→ Player HP with UI

Planned:
→ limited gameplay time
→ shooting and points
→ save system for high score
→ background