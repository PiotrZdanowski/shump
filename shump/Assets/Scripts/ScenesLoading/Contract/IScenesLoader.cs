﻿namespace QuasarBeam.ScenesLoading
{
    public interface IScenesLoader
    {
        public void LoadScene(int sceneId);
    }
}
