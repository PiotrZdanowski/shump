﻿using UnityEngine;

namespace QuasarBeam.ScenesLoading.Implementation
{
    [RequireComponent(typeof(IScenesLoader))]
    public class StartSceneLoader : MonoBehaviour
    {
        [SerializeField] private int startSceneId;

        private void Awake()
        {
            IScenesLoader loader = gameObject.GetComponent<IScenesLoader>();
            if(loader == null)
            {
                throw new System.Exception($"There is no {nameof(IScenesLoader)} script attached to the {gameObject.name} gameObject!");
            }
            loader.LoadScene(startSceneId);
        }
    }
}
