using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuasarBeam.ScenesLoading.Implementation
{
    internal class ScenesLoader : MonoBehaviour, IScenesLoader
    {
        private const float ALLOWING_SCENE_ACTIVATION_PROGRESS = 0.9f;

        private Coroutine loadingSceneCoroutine = null;

        public void LoadScene(int sceneId)
        {
            if(loadingSceneCoroutine != null)
            {
                return;
            }
            loadingSceneCoroutine = StartCoroutine(LoadSceneAsync(sceneId));
        }

        private IEnumerator LoadSceneAsync(int sceneId)
        {
            AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(sceneId);
            loadingOperation.allowSceneActivation = false;
            while(loadingOperation.progress >= ALLOWING_SCENE_ACTIVATION_PROGRESS)
            {
                yield return null;
            }
            loadingOperation.allowSceneActivation = true;
        }
    }
}