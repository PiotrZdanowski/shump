using QuasarBeam.ScenesLoading;
using UnityEngine;

namespace Shump
{
    [RequireComponent(typeof(IScenesLoader))]
    public class MainMenuInputHandler : MonoBehaviour
    {
        [SerializeField] private int gameplaySceneId;

        private IScenesLoader scenesLoader;

        private void Awake()
        {
            scenesLoader = gameObject.GetComponent<IScenesLoader>();
            if (scenesLoader == null)
            {
                this.enabled = false;
                throw new System.Exception($"No {nameof(IScenesLoader)} script was found! Attach one to {gameObject.name} gameObject!");
            }
        }

        void Update()
        {
            if (Input.anyKeyDown)
            {
                scenesLoader.LoadScene(gameplaySceneId);
            }
        }
    }
}
