using UnityEngine;
using QuasarBeam.Characters.Movement;
using QuasarBeam.Math;

namespace Shump.Player
{
    [RequireComponent(typeof(IMovement2D))]
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private string movementInputAxis;

        private IMovement2D movement2D;

        private void Awake()
        {
            movement2D = GetComponent<IMovement2D>();
            if (movement2D == null)
            {
                Debug.LogError($"No {nameof(IMovement2D)} script found on {this.gameObject.name}!");
            }
        }

        private void Update()
        {
            float movementInputValue = Input.GetAxis(movementInputAxis);
            movement2D.MoveInDirection(Percent.From01Value(0), Percent.From01Value(movementInputValue));
        }
    }
}
