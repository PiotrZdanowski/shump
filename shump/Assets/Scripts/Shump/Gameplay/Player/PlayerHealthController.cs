﻿using QuasarBeam.Characters.Health;
using Shump.UI.Health;
using UnityEngine;

namespace Shump.Player
{
    internal class PlayerHealthController : MonoBehaviour
    {
        [SerializeField] private GameObject playerObject;
        [SerializeField] private HealthUI playerHealthUI;

        private IHealth playerHealth;

        private void Awake()
        {
            playerHealth = playerObject.GetComponent<IHealth>();
            playerHealth.OnDamaged += OnPlayerDamaged;
        }

        private void Start()
        {
            playerHealthUI.Show(new HealthUIData(playerHealth.CurrentHealth, playerHealth.MaxHealth));
        }

        private void OnPlayerDamaged(IDamager damager, IHealth healthObject)
        {
            Debug.Log($"Player damaged. Current health: {playerHealth.CurrentHealth}/{playerHealth.MaxHealth}");

            if(healthObject.CurrentHealth <= 0)
            {
                Destroy(playerObject);
            }
            playerHealthUI.Show(new HealthUIData(playerHealth.CurrentHealth, playerHealth.MaxHealth));
        }
    }
}
