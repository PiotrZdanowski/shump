﻿using System;
using UnityEngine;
using Zenject;

namespace Shump.Enemies
{
    public class Enemy : MonoBehaviour, IPoolable<Vector3, IMemoryPool>, IDisposable
    {
        private IMemoryPool _pool;
        private bool disposed = false;

        public void Dispose()
        {
            if (disposed) return;
            _pool.Despawn(this);
            disposed = true;
        }

        public void OnDespawned()
        {
            gameObject.SetActive(false);
            _pool = null;
        }

        public void OnSpawned(Vector3 spawnPosition, IMemoryPool pool)
        {
            transform.position = spawnPosition;
            _pool = pool;
            disposed = false;
            gameObject.SetActive(true);
        }

        internal void Destroy()
        {
            Dispose();
        }

        public class Factory : PlaceholderFactory<Vector3, Enemy>
        {
        }
    }
}
