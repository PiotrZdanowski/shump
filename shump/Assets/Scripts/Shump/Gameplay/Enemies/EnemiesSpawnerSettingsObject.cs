﻿using UnityEngine;

namespace Shump.Enemies
{
    [CreateAssetMenu(fileName = "SpawnerSettings", menuName = "Shump/EnemiesSpawner/Settings")]
    internal class EnemiesSpawnerSettingsObject : ScriptableObject
    {
        [SerializeField] private EnemiesSpawnerSettings spawnerSettings;
        [SerializeField] private Enemy enemyPrefab;

        public EnemiesSpawnerSettings SpawnerSettings => spawnerSettings;
        public Enemy EnemyPrefab => enemyPrefab;
    }
}
