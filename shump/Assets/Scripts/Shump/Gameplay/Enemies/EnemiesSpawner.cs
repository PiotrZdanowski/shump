using QuasarBeam.Math;
using UnityEngine;
using Zenject;

namespace Shump.Enemies
{
    public class EnemiesSpawner : MonoBehaviour, ITickable, IInitializable
    {
        [Inject] private EnemiesSpawnerSettings settings;
        [Inject] private Enemy.Factory enemiesFactory;

        [SerializeField] private float timeToNextSpawn = 0;
        [SerializeField] private Transform[] spawnPositions;

        private System.Random random;

        public void Initialize()
        {
            timeToNextSpawn = settings.InitialSpawnTime;
            random = new System.Random();
        }

        public void Tick()
        {
            timeToNextSpawn -= Time.deltaTime;
            if (timeToNextSpawn < 0)
            {
                SpawnNewEnemy();
                timeToNextSpawn = settings.WaveSpawnTimeOffset;
            }
        }

        private void SpawnNewEnemy()
        {
            Transform spawnPosition = random.FromArray(spawnPositions);
            enemiesFactory.Create(spawnPosition.position);
            Debug.Log($"Created enemy at {spawnPosition.gameObject.name}, {spawnPosition.position}");
        }
    }
}
