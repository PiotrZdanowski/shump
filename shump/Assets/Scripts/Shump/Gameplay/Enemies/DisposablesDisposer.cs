﻿using System;
using UnityEngine;

namespace Shump.Enemies
{
    /// <summary>
    /// This disposables disposer disposes any disposable :>
    /// </summary>
    internal class DisposablesDisposer : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.TryGetComponent(out IDisposable disposable))
            {
                Dispose(collision.gameObject.name, disposable);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out IDisposable disposable))
            {
                Dispose(collision.otherCollider.name, disposable);
            }
        }

        private void Dispose(string name, IDisposable disposable)
        {
            disposable.Dispose();
            Debug.Log($"{gameObject.name} disposed {name}");
        }
    }
}
