﻿using UnityEngine;
using Zenject;

namespace Shump.Enemies
{
    internal class EnemiesSpawnerInstaller : MonoInstaller
    {
        [SerializeField] private EnemiesSpawnerSettingsObject settingsObject;
        [SerializeField] private Transform enemiesParent;
        [SerializeField] private EnemiesSpawner spawner;

        public override void InstallBindings()
        {
            Container.Bind<EnemiesSpawnerSettings>().FromInstance(settingsObject.SpawnerSettings);
            Container.BindFactory<Vector3, Enemy, Enemy.Factory>().FromMonoPoolableMemoryPool(
                (poolBindGenerator) => poolBindGenerator
                .FromComponentInNewPrefab(settingsObject.EnemyPrefab)
                .UnderTransform(enemiesParent)
                );
            Container.BindInterfacesTo<EnemiesSpawner>().FromInstance(spawner);
        }
    }
}
