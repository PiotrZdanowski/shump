using System;
using UnityEngine;

namespace Shump.Enemies
{
    [Serializable]
    public class EnemiesSpawnerSettings
    {
        [field: SerializeField] public float WaveSpawnTimeOffset { get; private set; }
        [field: SerializeField] public float InitialSpawnTime { get; private set; }
    }
}
