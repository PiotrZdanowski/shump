﻿using QuasarBeam.Characters.Health;
using QuasarBeam.ScenesLoading;
using System;
using System.Collections;
using UnityEngine;

namespace Shump.Gameplay
{
    internal class GameplayController : MonoBehaviour
    {
        [SerializeField] private GameObject player;
        [SerializeField] private GameObject gameOverUIText;
        [SerializeField] private float returnToMainMenuTime;
        [SerializeField] private int mainMenuSceneId;

        private IHealth playerHealth;
        private IScenesLoader scenesLoader;

        private void Awake()
        {
            playerHealth = player.GetComponent<IHealth>();
            playerHealth.OnDamaged += OnPlayerDamaged;
            gameOverUIText.SetActive(false);
            scenesLoader = GetComponent<IScenesLoader>();
        }

        private void OnPlayerDamaged(IDamager damager, IHealth healthObject)
        {
            if(playerHealth.CurrentHealth <= 0)
            {
                StartCoroutine(EndGame());
            }
        }

        private IEnumerator EndGame()
        {
            gameOverUIText.SetActive(true);
            float timeTillMainMenu = returnToMainMenuTime;
            while(timeTillMainMenu > 0)
            {
                Time.timeScale = timeTillMainMenu / returnToMainMenuTime;
                yield return null;
                timeTillMainMenu -= Time.unscaledDeltaTime;
            }
            Time.timeScale = 1;
            scenesLoader.LoadScene(mainMenuSceneId);
        }
    }
}
