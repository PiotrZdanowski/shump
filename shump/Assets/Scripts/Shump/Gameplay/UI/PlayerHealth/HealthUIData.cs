﻿namespace Shump.UI.Health
{
    public class HealthUIData
    {
        public readonly float currentHealth;
        public readonly float maxHealth;

        public HealthUIData(float currentHealth, float maxHealth)
        {
            this.currentHealth = currentHealth;
            this.maxHealth = maxHealth;
        }
    }
}