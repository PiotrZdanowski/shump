﻿using UnityEngine;
using UnityEngine.UI;

namespace Shump.UI.Health
{
    public class HealthUI : MonoBehaviour
    {
        [SerializeField] private Image fill;

        public void Show(HealthUIData data)
        {
            fill.fillAmount = data.currentHealth / data.maxHealth;
        }
    }
}
