using QuasarBeam.Characters.Movement;
using QuasarBeam.Math;
using UnityEngine;

namespace QuasarBeam.Characters.AI.Movement
{
    [RequireComponent(typeof(IMovement2D))]
    public class Single2DDirectionAIMovement : MonoBehaviour
    {
        [SerializeField] private Vector2 movementDirection = Vector2.left;

        private IMovement2D movement2D;

        private void Awake()
        {
            movement2D = GetComponent<IMovement2D>();
        }

        private void Update()
        {
            movement2D.MoveInDirection(Percent.From01Value(movementDirection.x), Percent.From01Value(movementDirection.y));
        }
    }
}

