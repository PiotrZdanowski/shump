using QuasarBeam.Math;
using System;
using UnityEngine;

namespace QuasarBeam.Characters.Movement.Implementation
{
    [RequireComponent(typeof(Rigidbody2D))]
    internal class SingleAxis2DMover : MonoBehaviour, IMovement2D
    {
        [field: SerializeField] public float MaxSpeed { get; set; } = 1f;
        [field: SerializeField] public Vector2 MovementDirection { get; set; } = Vector2.up;
        [field: SerializeField] public InputAxis UseInputAxis { get; set; } = InputAxis.Y;

        new private Rigidbody2D rigidbody;

        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody2D>();
        }

        public void MoveInDirection(Percent x, Percent y)
        {
            float speed = ResolveSpeedValue(x, y);
            rigidbody.velocity = speed * MovementDirection.normalized;
        }

        private float ResolveSpeedValue(Percent x, Percent y)
        {
            Percent speedPercent = UseInputAxis == InputAxis.X ? x : y;
            return speedPercent * MaxSpeed;
        }

        public enum InputAxis
        {
            X,
            Y
        }
    }
}
