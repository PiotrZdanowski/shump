﻿using QuasarBeam.Math;

namespace QuasarBeam.Characters.Movement
{
    public interface IMovement2D
    {
        /// <summary>
        /// Moves object in required direction
        /// </summary>
        /// <param name="x">Percent of max speed in the x axis</param>
        /// <param name="y">Percent of max speed in the y axis</param>
        public void MoveInDirection(Percent x, Percent y);

        public float MaxSpeed { get; set; }
    }
}
