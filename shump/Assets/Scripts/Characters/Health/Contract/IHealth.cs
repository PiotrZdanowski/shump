﻿namespace QuasarBeam.Characters.Health
{
    public interface IHealth
    {
        float CurrentHealth { get; }
        float MaxHealth { get; }

        event DamagedEventHandler OnDamaged;

        void Damage(IDamager damager, float damageValue);
    }

    public delegate void DamagedEventHandler(IDamager damager, IHealth healthObject);
}