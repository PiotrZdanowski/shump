using System;
using UnityEngine;

namespace QuasarBeam.Characters.Health
{
    internal class Health : MonoBehaviour, IHealth
    {
        [field: SerializeField] public float CurrentHealth { get; private set; }
        [field: SerializeField] public float MaxHealth { get; private set; }


        public event DamagedEventHandler OnDamaged = delegate { };

        private void Awake()
        {
            CurrentHealth = MaxHealth;
        }

        public void Damage(IDamager damager, float value)
        {
            CurrentHealth -= value;
            OnDamaged(damager, this);
        }
    }
}
