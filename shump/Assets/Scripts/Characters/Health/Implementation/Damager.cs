﻿using UnityEngine;

namespace QuasarBeam.Characters.Health
{
    internal class Damager : MonoBehaviour, IDamager
    {
        [field: SerializeField] public float DamageValue { get; set; }
        [field: SerializeField] public bool DestroyOnHit { get; set; }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.TryGetComponent(out IHealth health))
            {
                Damage(health);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.TryGetComponent(out IHealth health))
            {
                Damage(health);
            }
        }

        private void Damage(IHealth health)
        {
            health.Damage(this, DamageValue);
            if (DestroyOnHit)
            {
                Destroy(gameObject);
            }
        }
    }
}
