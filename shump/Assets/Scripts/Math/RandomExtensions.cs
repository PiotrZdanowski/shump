﻿using System;

namespace QuasarBeam.Math
{
    public static class RandomExtensions
    {
        public static T FromArray<T>(this Random random, T[] elements)
        {
            int randomIndex = random.Next(elements.Length);
            return elements[randomIndex];
        }
    }
}
