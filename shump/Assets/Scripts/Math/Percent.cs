using System;
using UnityEngine;

namespace QuasarBeam.Math
{
    [Serializable]
    public struct Percent
    {
        [SerializeField]
        private float value;

        /// <summary>
        /// Creates a new Percent struct
        /// </summary>
        /// <param name="value">Value of the percent. E.g. value 30 is converted to 30%</param>
        public Percent(float value)
        {
            this.value = value;
        }

        public override bool Equals(object obj)
        {
            bool isEqualToOtherPercent = (obj is Percent percent &&
                   value == percent.value);
            bool isEqualToOtherFloat = (obj is float f &&
                   Mathf.Approximately(f, this));
            return isEqualToOtherPercent || isEqualToOtherFloat;
        }

        public override int GetHashCode()
        {
            return -1584136870 + value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{value}%";
        }

        public Percent Inverted()
        {
            return Invert(this);
        }

        public Percent Clamped()
        {
            float newValue = value;
            if (newValue > 100)
            {
                newValue = 100;
            }
            if (newValue < 0)
            {
                newValue = 0;
            }
            return new Percent(newValue);
        }

        public static implicit operator float(Percent p)
        {
            return p.value / 100f;
        }

        public static Percent FromFraction(float numerator, float denominator)
        {
            return new Percent(numerator * 100 / denominator);
        }

        public static Percent From01Value(float value01)
        {
            return new Percent(value01 * 100);
        }

        public static Percent Invert(Percent percent)
        {
            return new Percent(-(percent.value - 100));
        }

        public static Percent operator *(Percent one, Percent two)
        {
            return new Percent((float)one * two * 100f);
        }

        public static Percent operator *(Percent one, float f)
        {
            return new Percent(((float)one * f) * 100f);
        }

        public static Percent operator +(Percent one, Percent two)
        {
            return new Percent(((float)one + two) * 100f);
        }

        public static Percent operator -(Percent one, Percent two)
        {
            return new Percent(((float)one - two) * 100f);
        }

        public static Percent operator -(Percent one)
        {
            return new Percent((-(float)one) * 100f);
        }
    }
}